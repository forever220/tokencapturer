﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Xbox.Controls;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xbox.Input;
using System.Linq;
using Microsoft.Xbox.Security.XAuth;

namespace RSTokenCapturerApplication
{
    public partial class MainPage : XboxApplicationPage
    {
        GridFocusHelper focusHelper;
        public MainPage()
        {
            InitializeComponent();

            this.Loaded += (sender, args) =>
            {
                focusHelper = new GridFocusHelper(LayoutRoot);
                //LayoutRoot.Children.OfType<Button>().First().Focus();
                this.btnGetToken.IsEnabled = true;
                this.btnGetToken.Focus();

                this.btnGetToken.Click += new RoutedEventHandler(btnGetToken_Click);

                this.txtEndpoint.Text = "https://xboxdev.istreamplanet.cn/";
            };
        }

        void Log(string msg)
        {
            this.txtToken.Text += msg + "\r\n";
            Console.WriteLine(msg);
        }

        void btnGetToken_Click(object sender, RoutedEventArgs e)
        {
            if (!this.SignIn())
            {
                return;
            }
            this.lblError.Text = "";
            string url = this.txtEndpoint.Text;
            if (string.IsNullOrEmpty(url) || !url.StartsWith("http"))
            {
                this.lblError.Text = "Invalid STS Endpoint.";
                return;
            }

            try
            {

                this.GetTokenAsync((int)Gamer.SignedInGamers[0].PlayerIndex, new Uri(url), token =>
                {
                    if (!string.IsNullOrEmpty(token))
                    {
                        Log("========== Token Begin =============");
                        Log(token);
                        Log("========== Token End ===============");
                    }
                });
            }catch(Exception exp){
                Log(exp.Message);
            }
        }

        private bool SignIn()
        {
            if (Gamer.SignedInGamers != null & Gamer.SignedInGamers.Count > 0)
            {
                return true;
            }

            SignedInGamer.ShowSignIn(1, true);
            return false;
        }


        public void GetTokenAsync(int playerIndex, Uri urn, Action<string> callback)
        {
            XAuthService.Start(false);

            XAuthTokenRequest request = new XAuthTokenRequest();
            request.Completed += (obj, args) => OnXAuthTokenRequestCompleted(request,callback, args);
            
            try
            {
                Log("GetTokenAsync: playerIndex=" + playerIndex.ToString() + " urn=" + (urn == null ? "(null)" : urn.ToString()));

                request.GetTokenAsync(playerIndex, urn);
            }
            catch (Exception ex)
            {
                Log(ex.Message);
                callback(null);
            }
        }
        private void OnXAuthTokenRequestCompleted(XAuthTokenRequest request, Action<string> callback, XAuthCompletedEventArgs args)
        {
            
            if (callback == null)
            {
                Log("OnXAuthTokenRequestCompleted:  null callback action passed in!");
                return;
            }
            if (args.Token != null)
            {
                Log("OnXAuthTokenRequestCompleted: args.Token=" + ((long)args.Token.UserId).ToString());
                string token = new string(args.Token.GetBytes().Select(Convert.ToChar).ToArray());
                callback(token);
            }
            else
            {
                Log("OnXAuthTokenRequestCompleted: null token returned ");
                XAuthService.ReleaseTokens(); // attempt to clear the cache
                callback(null);
            }
        }
    }
}
